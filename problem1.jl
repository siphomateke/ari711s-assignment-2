### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 9f5095c4-1975-40f0-a119-eb4e1914f2ca
using DataStructures, PlutoUI, Crayons

# ╔═╡ 04a0d796-ba3a-11ec-2b99-bf99a9174234
md"
# ARI711S - Assignment 2
## Problem 1
"

# ╔═╡ c6d07937-67c3-4139-80db-4b5ed201993e
mutable struct Location
	floor::Int8
	office::Int8
end

# ╔═╡ 63c0b84c-2cb2-4051-aceb-c70e255c570a
struct State
	parcels::Matrix{Int8}
	agent::Location
end

# ╔═╡ 40eaf69b-ba51-4f86-a3b8-1254315a6c9b
begin
	max_iterations = 10000
	num_floors = 4
	offices_per_floor = 5
	parcels = zeros(Int8, num_floors, offices_per_floor)
	parcels[1,1] = 4
	parcels[1,5] = 1
	parcels[2,2] = 2
	parcels[4,5] = 1
	parcels[4,4] = 2
	parcels[4,3] = 1
	initial_state = State(parcels, Location(1,1))
end

# ╔═╡ 0225856f-ae56-4734-9687-33c053397c27
function get_room_number(floor, office_position)
	if office_position > offices_per_floor
		throw("Failed to get room number for office $office_position on floor $floor as there are only $offices_per_floor per floor")
	end
	if floor > num_floors
		throw("Failed to get room number for office $office_position on floor $floor as there are only $num_floors floors")
	end
	return ((floor - 1) * offices_per_floor) + office_position
end

# ╔═╡ 5fedb403-a12b-4bac-b652-5687ab0c7fe3
@enum Action me mw mu md co

# ╔═╡ 7f7d3eee-9546-437f-a846-e502436d82f1
action_costs = Dict{Action, Int8}(
	me => 2,
	mw => 2,
	mu => 1,
	md => 1,
	co => 5
)

# ╔═╡ 6a3bfea1-347b-409f-985f-f14b30ed2b3a
action_names = Dict{Action, String}(
	me => "Move East",
	mw => "Move West",
	mu => "Move Up",
	md => "Move Down",
	co => "Collect"
)

# ╔═╡ 622480cb-9c11-47ed-8861-c591e0b1c082
mutable struct Node
	state::State
	parent::Union{Nothing, Node}
	action::Union{Nothing, Action}
	path_cost::Int64
	f_score::Int64
end

# ╔═╡ 8f5fe081-59d3-4026-beae-654bebb1d6c2
function create_transition(state, parent, action)
	h = sum(state.parcels) * 3
	g = parent.path_cost + action_costs[action]
	return Node(state, parent, action, g, g + h)
end

# ╔═╡ a944dd49-b5e9-432d-95b6-ae6d365d4992
function get_transitions(parent::Node)
	state = parent.state
	transitions::Array{Node} = []

	# Move East
	if state.agent.office < offices_per_floor
		new_state = deepcopy(state)
		new_state.agent.office += 1
		push!(transitions, create_transition(new_state, parent, me))
	end
	
	# Move West
	if state.agent.office > 1
		new_state = deepcopy(state)
		new_state.agent.office -= 1
		push!(transitions, create_transition(new_state, parent, mw))
	end
	
	# Move Up
	if state.agent.floor < num_floors
		new_state = deepcopy(state)
		new_state.agent.floor += 1
		push!(transitions, create_transition(new_state, parent, mu))
	end
	
	# Move Down
	if state.agent.floor > 1
		new_state = deepcopy(state)
		new_state.agent.floor -= 1
		push!(transitions, create_transition(new_state, parent, md))
	end
	
	# Collect
	if state.parcels[state.agent.floor, state.agent.office] > 0
		new_state = deepcopy(state)
		new_state.parcels[state.agent.floor, state.agent.office] -= 1
		push!(transitions, create_transition(new_state, parent, co))
	end

	return transitions
end

# ╔═╡ 5eaa9284-f85a-40ff-b01d-342fe80e5c98
function is_goal(state::State)
	return sum(state.parcels) == 0
end

# ╔═╡ 55729f66-6ec6-4d1b-a198-428665830a7c
function get_path(node::Node)
	path = [node]
	while !isnothing(node.parent)
		node = node.parent
		pushfirst!(path, node)
	end
	return path
end

# ╔═╡ cb839de8-0835-4ef3-a3ea-66d8e4f050c1
function visualize_path(path)
	for i in 1:length(path)
		node = path[i]
		output = ""
		if !isnothing(node.action)
			action = action_names[node.action]
			if node.action != co && i < length(path)
				next_node = path[i+1]
				action *= " to floor $(next_node.state.agent.floor) office $(next_node.state.agent.office) room $(get_room_number(next_node.state.agent.floor, next_node.state.agent.office))"
			end
			output *= "\t|\n\t|\n$(action)\n\t|\n\t|\n\tV \n\n"
		end
		for floor in reverse(1:num_floors)
			for office in 1:offices_per_floor
				char = string(node.state.parcels[floor, office])
				if node.state.agent.floor == floor && node.state.agent.office == office
					output *= string(Crayon(foreground = :green))
				end
				output *= "$(char) "
				output *= string(Crayon(foreground = :white))
			end
			output *= "\n"
		end
		println(output)
	end
end

# ╔═╡ 8a03025f-d5c4-4fd3-855e-4606c4925e00
function solution(node::Node, explored::Array{Node})
	path = get_path(node)
	actions = []
	rooms = []
	for node in path
		if !isnothing(node.action)
			push!(actions, node.action)
			push!(rooms, get_room_number(node.state.agent.floor, node.state.agent.office))
		end
	end

	println("Found $(length(actions)) step solution with cost $(node.path_cost) in $(length(explored)) iterations")
	println("Actions: $(join(actions, " -> "))")
	println("Room order: $(join(rooms, " -> "))")
	println("\n===\n")
	visualize_path(path)
end

# ╔═╡ 27d7d27c-df68-4390-8166-8542d7e3e748
function failure(message::String)
	println(string(Crayon(foreground = :red)) * message)
end

# ╔═╡ 1a332f64-dc4c-4eac-82d4-bcc23f9a2e03
function find_node_in_list(node::Node, list)::Union{Nothing, Node}
	for n in list
		if n.state.parcels == node.state.parcels && n.state.agent.floor == node.state.agent.floor && n.state.agent.office == node.state.agent.office
			return n
		end
	end
	return nothing
end

# ╔═╡ 03a83714-08ea-44b2-8715-05c08db7377a
function node_in_list(node::Node, list)
	return !isnothing(find_node_in_list(node, list))
end

# ╔═╡ 76f71993-fd3e-49fb-9b6d-3e1925d9268b
function a_star_search(start::State)
	node = Node(start, nothing, nothing, 0, 0)
	frontier = PriorityQueue{Node, Int}()
	enqueue!(frontier, node, node.f_score)
	explored::Array{Node} = []
	iteration = 0
	while !isempty(frontier)
		node = dequeue!(frontier)
		if is_goal(node.state)
			return solution(node, explored)
		end
		push!(explored, node)
		
		for child in get_transitions(node)
			if !node_in_list(child, explored)
				existing = find_node_in_list(child, keys(frontier))
				if isnothing(existing)
					enqueue!(frontier, child, child.f_score)	
				elseif child.path_cost < existing.path_cost
					existing.path_cost = child.path_cost
					existing.parent = child.parent
					existing.action = child.action
				end
			end
		end
		iteration += 1
		if iteration > max_iterations
			return failure("Timed out")
		end
	end
	return failure("Failed to find solution after exploring all nodes")
end

# ╔═╡ 91ac051a-5a5c-4aa6-a5b2-adcef313f069
with_terminal() do
	a_star_search(initial_state)
end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Crayons = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
Crayons = "~4.1.1"
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "621f4f3b4977325b9128d5fae7a8b4829a0c2222"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.4"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╟─04a0d796-ba3a-11ec-2b99-bf99a9174234
# ╠═9f5095c4-1975-40f0-a119-eb4e1914f2ca
# ╠═40eaf69b-ba51-4f86-a3b8-1254315a6c9b
# ╟─0225856f-ae56-4734-9687-33c053397c27
# ╠═c6d07937-67c3-4139-80db-4b5ed201993e
# ╠═63c0b84c-2cb2-4051-aceb-c70e255c570a
# ╠═5fedb403-a12b-4bac-b652-5687ab0c7fe3
# ╠═7f7d3eee-9546-437f-a846-e502436d82f1
# ╠═6a3bfea1-347b-409f-985f-f14b30ed2b3a
# ╠═622480cb-9c11-47ed-8861-c591e0b1c082
# ╠═8f5fe081-59d3-4026-beae-654bebb1d6c2
# ╠═a944dd49-b5e9-432d-95b6-ae6d365d4992
# ╠═5eaa9284-f85a-40ff-b01d-342fe80e5c98
# ╟─55729f66-6ec6-4d1b-a198-428665830a7c
# ╟─cb839de8-0835-4ef3-a3ea-66d8e4f050c1
# ╟─8a03025f-d5c4-4fd3-855e-4606c4925e00
# ╟─27d7d27c-df68-4390-8166-8542d7e3e748
# ╠═1a332f64-dc4c-4eac-82d4-bcc23f9a2e03
# ╠═03a83714-08ea-44b2-8715-05c08db7377a
# ╠═76f71993-fd3e-49fb-9b6d-3e1925d9268b
# ╠═91ac051a-5a5c-4aa6-a5b2-adcef313f069
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
